package com.mile1.service;

import com.mile1.bean.*;

public class StudentService {
	public int findNumberOfNullMarksArray(Student s[]) {
		int markscount = 0;
		for (int i = 0; i < s.length; i++) {
			try{
				s[i].getMarks();
			} 
			catch(Exception e){
				markscount++;
			}
		}
		return markscount;
	}

	public int findNumberOfFullName(Student s[]) {
		int namecount = 0;
		for (int i = 0; i < s.length; i++) {
			try{
				s[i].getMarks();
			} catch(Exception e) {
				namecount++;
			}
		}
		return namecount;
	}

	public int findNumberOfNullObjects(Student s[]) {
         int objectcount=0;
         for(int i=0;i<s.length;i++){
        	 if(s[i]==null){
        		 objectcount++;
        	 }
         }
         return objectcount;
	}
}
