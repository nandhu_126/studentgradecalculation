package com.mile1.service;

import com.mile1.bean.*;
import com.mile1.exception.*;

public class StudentReport {
	public String findGrades(Student s) {
		int marks[] = s.getMarks();
		int sum = 0;
		for (int i = 0; i < marks.length; i++) {
			if (marks[i] < 35) {
				return "F";
			} else {
				sum += marks[i];
			}
		}
		if(sum<125){
			return "D";
		}
		else if (sum < 150) {
			return "C";
		} else if (sum < 200) {
			return "B";
		} else if (sum < 250) {
			return "A";
		} else {
			return "A+";
		}

	}
	public String validate(Student s)throws NullMarksArrayException,NullNameException,NullStuddentObjectExcption{
		if(s==null){
			throw new NullStuddentObjectExcption();
		}
		else{
			if(s.getName()==null)throw new NullNameException();
			
			else if(s.getMarks()==null)throw new NullMarksArrayException();
			return findGrades(s);
		}
	}
}
