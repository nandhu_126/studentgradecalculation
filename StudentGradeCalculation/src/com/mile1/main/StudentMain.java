package com.mile1.main;

import com.mile1.bean.*;
import com.mile1.exception.*;
import com.mile1.service.*;

public class StudentMain {
	static Student s[] = new Student[4];

	static {
		for (int i = 0; i < s.length; i++)
			s[i] = new Student();
		s[0] = new Student("Sekar", new int[] { 35, 35, 35 });
		s[1] = new Student(null, new int[] { 11, 22, 33 });
		s[2] = null;
		s[3] = new Student("Manoj", null);

	}

	public static void main(String[] args) {
		StudentReport sr = new StudentReport();
		StudentService ss = new StudentService();
		String x = null;

		for (int i = 0; i < s.length; i++) {
			try {
				x = sr.validate(s[i]);
			} catch (NullNameException e) {
				x = "NullNameException occured";
			} catch (NullMarksArrayException e) {
				x = "NullMarksArrayException occured";
			} catch (NullStuddentObjectExcption e) {
				x = "NullStudentException occured";
			}

			System.out.println("GRADE = " + x);
		}

		System.out.println("Name null count:" + ss.findNumberOfFullName(s));
		System.out.println("Mark null count:" + ss.findNumberOfNullMarksArray(s));
		System.out.println("Object null count:" + ss.findNumberOfNullObjects(s));

	}

}
